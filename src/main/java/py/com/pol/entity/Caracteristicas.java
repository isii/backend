package py.com.pol.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "caracteristicas")
public class Caracteristicas implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_caracteristica", nullable = false)
    private Integer idCaracteristica;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre", nullable = false, length = 50)
    private String nombre;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "eliminado", nullable = false)
    private Boolean eliminado;
    
    @JoinColumn(name = "id_tipo_item", referencedColumnName = "id_tipo_item", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private TipoItem idTipoItem;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idCaracteristica", fetch = FetchType.EAGER)
    private List<CaracteristicasItem> caracteristicasItemList;

}
