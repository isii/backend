package py.com.pol.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "caracteristicas_item")
public class CaracteristicasItem implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_caracteristica_item", nullable = false)
    private Integer idCaracteristicaItem;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "valor", nullable = false, length = 100)
    private String valor;

    @JoinColumn(name = "id_item", referencedColumnName = "id_item", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)    
    private Items item;
    
    @JoinColumn(name = "id_caracteristica", referencedColumnName = "id_caracteristica", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Caracteristicas idCaracteristica;
    
}

