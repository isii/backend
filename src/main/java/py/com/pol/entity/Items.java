package py.com.pol.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "items")
public class Items implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_item", nullable = false)
    private Integer idItem;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 80)
    @Column(name = "titulo", nullable = false, length = 80)
    private String titulo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 240)
    @Column(name = "descripcion", nullable = false, length = 240)
    private String descripcion;    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_creacion", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "estado", nullable = false, length = 1)
    private String estado;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "complejidad", nullable = false)
    private Integer complejidad;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "version", nullable = false)
    private Integer version;    
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "eliminado", nullable = false)
    private Boolean eliminado;
    
    @Column(name = "root", nullable = true)
    private Boolean root;    
    
    @JoinColumn(name = "id_proyecto", referencedColumnName = "id_proyecto", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Proyectos idProyecto;
    
    @JoinColumn(name = "sucesor", referencedColumnName = "id_item", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private Items sucesor;
    
    @JoinColumn(name = "antecesor", referencedColumnName = "id_item", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private Items antecesor;
    
    @JoinColumn(name = "padre", referencedColumnName = "id_item", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private Items padre;
    
    @JoinColumn(name = "hijo", referencedColumnName = "id_item", nullable = true)
    @ManyToOne(optional = true, fetch = FetchType.EAGER)
    private Items hijo;    
    
    @JoinColumn(name = "creador", referencedColumnName = "id_usuario", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuario creador;
    
    @JoinColumn(name = "id_tipo_item", referencedColumnName = "id_tipo_item", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private TipoItem idTipoItem;    
    
    @JoinTable(name = "linea_base_items", joinColumns = {
            @JoinColumn(name = "id_item", referencedColumnName = "id_item")}, inverseJoinColumns = {
            @JoinColumn(name = "id_linea_base", referencedColumnName = "id_linea_base")})
    @ManyToMany
    private List<LineaBase> lineasBases;    
    
}
