package py.com.pol.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import py.com.pol.entity.Proyectos;

@Repository
public interface ProyectoRepository extends CrudRepository<Proyectos, Integer> {

    @Query("SELECT DISTINCT P FROM Proyectos P "
            + "JOIN P.rolesProyectoList RP "
            + "JOIN RP.idUsuario U "
            + "WHERE P.eliminado IS FALSE AND "
            + "RP.eliminado IS FALSE "
            + "AND U.eliminado IS FALSE "
            + "AND U.idUsuario = :user")
    public List<Proyectos> findAll(@Param("user") Integer userId);

    @Query(nativeQuery = true,
            value = "SELECT COUNT(P.id_proyecto) FROM proyectos P\n"
            + "WHERE P.eliminado = FALSE")
    Long getQuantityProjects();

    @Query(nativeQuery = true,
            value = "SELECT P.id_proyecto,\n"
            + "(SELECT COUNT(IT.id_item) FROM items IT\n"
            + "    WHERE IT.id_proyecto = P.id_proyecto AND IT.eliminado = FALSE) AS CANT_TAREA\n"
            + "FROM items I\n"
            + "        JOIN proyectos P\n"
            + "            ON P.id_proyecto = I.id_proyecto\n"
            + "WHERE P.eliminado = FALSE\n"
            + "GROUP BY P.id_proyecto")
    List<Object[]> numberOfTasksPerProject();

    @Query(nativeQuery = true,
            value = "SELECT COUNT(PRO.id_proyecto)\n"
            + "FROM proyectos PRO\n"
            + "WHERE PRO.eliminado = FALSE AND PRO.estado = 'F'")
    Long numberOfCompletedProjects();

    @Query(nativeQuery = true,
            value = "SELECT PRO.nombre, PRO.fecha_creacion\n"
            + "FROM proyectos PRO\n"
            + "WHERE PRO.eliminado = FALSE\n"
            + "ORDER BY PRO.fecha_creacion ASC")
    List<Object[]> getProjectsTimeLine();
}
