package py.com.pol.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import py.com.pol.entity.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    @Query("SELECT U FROM Usuario U WHERE U.email = :e AND U.password = :p AND U.eliminado = FALSE")
    Usuario findByEmailPassword(@Param("e") String email,
            @Param("p") String password);

    @Query("SELECT U FROM Usuario U WHERE U.username = :e AND U.password = :p AND U.eliminado = FALSE")
    Usuario findByUsernamePassword(@Param("e") String email,
            @Param("p") String password);

    @Query("SELECT U FROM Usuario U WHERE U.email = :e AND U.eliminado = FALSE")
    Usuario findByEmail(@Param("e") String email);

    @Query("SELECT U FROM Usuario U WHERE U.email = :e AND U.accessToken = :a AND U.eliminado = FALSE")
    Usuario findByEmailAccessToken(@Param("e") String email,
            @Param("a") String accessToken);

    @Query("SELECT U FROM Usuario U WHERE U.accessToken = :a AND U.eliminado = FALSE")
    Usuario findByAccessToken(@Param("a") String accessToken);

    @Override
    @Query("SELECT U FROM Usuario U WHERE U.eliminado = FALSE")
    List<Usuario> findAll();

    @Query(nativeQuery = true,
            value = "SELECT COUNT(U.id_usuario) FROM usuarios U WHERE U.eliminado = FALSE")
    Long getQuantityUsers();

    @Query(nativeQuery = true,
            value = "SELECT (SUM(CASE WHEN RS.id_rol = 1 THEN 1 ELSE 0 END)*100)/COUNT(U.id_usuario) AS CANT_ADM,\n"
            + "       (SUM(CASE WHEN RS.id_rol = 2 THEN 1 ELSE 0 END)*100)/COUNT(U.id_usuario) AS CANT_USR\n"
            + "FROM usuarios U\n"
            + "    JOIN roles_sistema RS ON RS.id_rol = U.id_rol_sistema\n"
            + "WHERE U.eliminado = FALSE")
    List<Object[]> percentageOfUsersByRole();

}
