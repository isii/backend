package py.com.pol.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import py.com.pol.entity.TipoItem;


@Repository
public interface TipoItemRepository extends CrudRepository<TipoItem, Integer> {

    
}
