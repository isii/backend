package py.com.pol.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import py.com.pol.entity.RolesSistema;

@Repository
public interface RolSistemaRepository extends CrudRepository<RolesSistema, Integer> {
    
}
