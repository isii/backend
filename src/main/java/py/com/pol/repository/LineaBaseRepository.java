package py.com.pol.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import py.com.pol.entity.LineaBase;


@Repository
public interface LineaBaseRepository extends CrudRepository<LineaBase, Integer> {

    @Query("SELECT L FROM LineaBase L JOIN L.idProyecto P "
            + "WHERE P.idProyecto = :proyecto")
    public List<LineaBase> findAll(@Param("proyecto") Integer projectId);
    
}
