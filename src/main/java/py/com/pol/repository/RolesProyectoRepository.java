package py.com.pol.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import py.com.pol.entity.RolesProyecto;

@Repository
public interface RolesProyectoRepository extends CrudRepository<RolesProyecto, Integer> {

    @Query("SELECT R FROM RolesProyecto R JOIN R.proyectos P WHERE P.idProyecto = :proyecto AND R.eliminado IS FALSE")
    public List<RolesProyecto> findAll(@Param("proyecto") Integer projectId);
    
}
