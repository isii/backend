package py.com.pol.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import py.com.pol.entity.Rol;

@Repository
public interface RolRepository extends CrudRepository<Rol, Integer> {

    @Query("SELECT R FROM Rol R WHERE R.codigo = :code")
    public Rol findByCode(@Param("code") String code);
    
}
