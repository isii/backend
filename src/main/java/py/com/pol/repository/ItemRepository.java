package py.com.pol.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import py.com.pol.entity.Items;


@Repository
public interface ItemRepository extends CrudRepository<Items, Integer> {

    @Query("SELECT I FROM Items I JOIN I.idProyecto P "
            + "WHERE I.eliminado IS FALSE AND P.idProyecto = :proyecto")
    public List<Items> findAll(@Param("proyecto") Integer projectId);
    
    @Query("SELECT I FROM Items I "
            + "WHERE I.eliminado IS FALSE AND I.padre.idItem = :task")    
    public List<Items> findChildrens(@Param("task") Integer taskId);
    
    @Query("SELECT I FROM Items I JOIN I.idProyecto P "
            + "WHERE I.eliminado IS FALSE AND P.idProyecto = :project "
            + "AND I.root IS TRUE")
    public List<Items> findAllParents(@Param("project") Integer project);
    
}
