package py.com.pol.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import py.com.pol.entity.DatosPersonales;

@Repository
public interface DatosPersonalesRepository extends CrudRepository<DatosPersonales, Integer> {

}
