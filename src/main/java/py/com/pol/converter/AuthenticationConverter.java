package py.com.pol.converter;

import org.springframework.stereotype.Component;
import py.com.pol.entity.Usuario;
import py.com.pol.model.exception.ConverterException;
import py.com.pol.model.response.LoginResponse;

@Component
public class AuthenticationConverter {

    public LoginResponse buildEntityToModel(Usuario entity) throws ConverterException {
        try {
            if(entity == null){ return null; }
            LoginResponse model = new LoginResponse();
            model.setId(entity.getIdUsuario());
            model.setAccessToken(entity.getAccessToken());
            model.setAge(String.valueOf(entity.getFechaCreacion()));
            model.setEmail(entity.getEmail());
            model.setRol(entity.getIdRolSistema().getCodigo());
            model.setPhoto(entity.getIdDatosPersonales().getFoto());
            model.setUsername(entity.getUsername());
            return model;
        }
        catch(Exception ex){
            throw new ConverterException("Ha ocurrido un error en la conversión");
        }
    }
}
