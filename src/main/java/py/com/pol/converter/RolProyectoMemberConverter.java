package py.com.pol.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import py.com.pol.entity.Proyectos;
import py.com.pol.entity.Rol;
import py.com.pol.entity.RolesProyecto;
import py.com.pol.entity.Usuario;
import py.com.pol.model.request.RolesProyectoMiembrosRequest;
import py.com.pol.model.response.RolesProyectoMiembros;

/**
 *
 * @author Gino Junchaya
 */

@Component
public class RolProyectoMemberConverter {
    
    @Autowired
    private UsuarioConverter usuarioConverter;
    
    @Autowired    
    private RolConverter rolConverter;
    
    public RolesProyectoMiembros entityToModel(RolesProyecto entity){
        if(entity == null){ return null; }
        RolesProyectoMiembros model = new RolesProyectoMiembros();
        model.setFechaCreacion(entity.getFechaCreacion());
        model.setMiembro(usuarioConverter.entityToModelMin(entity.getIdUsuario()));
        model.setAutorizador(usuarioConverter.entityToModelMin(entity.getAutorizador()));
        model.setRol(rolConverter.entityToModel(entity.getRol()));
        model.setId(entity.getId());
        return model;
    }
    
    public List<RolesProyectoMiembros> entitiesToModels(List<RolesProyecto> entities){
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        List<RolesProyectoMiembros> models = new ArrayList<>();
        entities.forEach((entity) -> {
            models.add(entityToModel(entity));
        });
        return models;
    }
    
    public RolesProyecto modelToEntity(RolesProyectoMiembrosRequest model, Usuario autorizador, Boolean inserting){
        if(model == null){ return null; }
        RolesProyecto entity = new RolesProyecto();
        entity.setFechaCreacion(new Date());
        entity.setAutorizador(autorizador);
        
        Usuario miembro = new Usuario();
        miembro.setIdUsuario(model.getMiembro());
        entity.setIdUsuario(miembro);
        
        Rol rol = new Rol();
        rol.setIdRol(model.getRol());
        entity.setRol(rol);
        
        Proyectos proyecto = new Proyectos();
        proyecto.setIdProyecto(model.getProyecto());
        entity.setProyectos(proyecto);
                
        entity.setAutorizador(autorizador);
        entity.setEliminado(Boolean.FALSE);
        
        if(inserting){
            entity.setId(model.getId());
        }
        return entity;
    }
    
}
