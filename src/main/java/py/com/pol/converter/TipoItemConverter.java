package py.com.pol.converter;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import py.com.pol.entity.TipoItem;
import py.com.pol.model.request.TipoItemRequest;
import py.com.pol.model.response.TipoItemResponse;

/**
 *
 * @author Gino Junchaya
 */

@Component
public class TipoItemConverter {
    
    public TipoItemResponse entityToModel(TipoItem entity){
        if(entity == null){ return null; }
        TipoItemResponse model = new TipoItemResponse();
        model.setDescripcion(entity.getDescripcion());
        model.setId(entity.getIdTipoItem());
        return model;
    }
    
    public List<TipoItemResponse> entitiesToModels(List<TipoItem> entities){
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        List<TipoItemResponse> models = new ArrayList<>();
        entities.forEach((entity) -> {
            models.add(entityToModel(entity));
        });
        return models;
    }
    
    public TipoItem modelToEntity(TipoItemRequest model, Boolean inserting){
        if(model == null){ return null; }
        TipoItem entity = new TipoItem();
        entity.setDescripcion(model.getDescripcion());
        entity.setEliminado(Boolean.FALSE);
        if(inserting){
            entity.setIdTipoItem(model.getId());
        }
        return entity;
    }
    
}
