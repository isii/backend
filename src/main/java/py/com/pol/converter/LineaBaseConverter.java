package py.com.pol.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import py.com.pol.entity.Items;
import py.com.pol.entity.LineaBase;
import py.com.pol.entity.Proyectos;
import py.com.pol.entity.Usuario;
import py.com.pol.model.request.LineaBaseRequest;
import py.com.pol.model.response.LineaBaseResponse;
import py.com.pol.util.enums.LineaBaseStatus;

/**
 *
 * @author Gino Junchaya
 */

@Component
public class LineaBaseConverter {
    
    @Autowired
    private UsuarioConverter usuarioConverter;
    
    @Autowired
    private ItemsConverter itemsConverter;
    
    @Autowired
    private ProyectoConverter proyectoConverter;
    
    public LineaBase buildEntity(LineaBaseRequest model, List<Items> items, Usuario creador){
        LineaBase lineaBase = new LineaBase();
        lineaBase.setIdLineaBase(model.getId());
        lineaBase.setCreador(creador);
        lineaBase.setEstado(LineaBaseStatus.INICIADO.getStatus());
        lineaBase.setFechaCreacion(new Date());
        lineaBase.setItems(items);
        
        Proyectos proyecto = new Proyectos();
        proyecto.setIdProyecto(model.getProyecto());
        
        lineaBase.setIdProyecto(proyecto);
        
        return lineaBase;
    }
    
    public LineaBaseResponse entityToModel(LineaBase entity){
        if(entity == null){ return null; }
        LineaBaseResponse model = new LineaBaseResponse();
        model.setId(entity.getIdLineaBase());
        model.setCreador(usuarioConverter.entityToModelMin(entity.getCreador()));
        model.setEstado(entity.getEstado());
        model.setItems(itemsConverter.entitiesToModelsAlter(entity.getItems()));
        model.setProyecto(proyectoConverter.entityToModel(entity.getIdProyecto()));
        return model;
    }
    
    public List<LineaBaseResponse> entitiesToModels(List<LineaBase> entities){
        List<LineaBaseResponse> models = new ArrayList<>();
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        entities.stream()
        .forEach(
            entity -> models.add(entityToModel(entity))
        );
        return models;
    }
    
}
