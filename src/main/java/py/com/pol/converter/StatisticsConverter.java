package py.com.pol.converter;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;
import py.com.pol.model.response.Statistics;
import py.com.pol.model.response.TimeLineGraphing;
import py.com.pol.util.DateUtils;

@Component
public class StatisticsConverter {

    public Statistics buildStatistics(Long cantUsr, List<Object[]> percentUsrRol,
            Long cantProject, List<Object[]> projectPerTask, Long cantProjectFinish,
            List<Object[]> projectTimeLine) {
        Map<String, Object> mapUser = new HashMap<>();
        mapUser.put("cantidad_administradores_porcentaje", percentUsrRol.get(0)[0]);
        mapUser.put("cantidad_usuario_porcentaje", percentUsrRol.get(0)[1]);
        List<TimeLineGraphing> projectTimeLineList = projectTimeLine.stream().map(a
                -> TimeLineGraphing.builder().projectName(Objects.toString(a[0]))
                        .createDate(Objects.toString(DateUtils.ofTimestampToString(((java.sql.Timestamp) a[1]))))
                        .build()
        ).collect(Collectors.toList());
        return Statistics.builder()
                .cantUsr(cantUsr)
                .mapUser(mapUser)
                .cantProject(cantProject)
                .averageTask(projectPerTask.stream().map(arg0 -> new BigDecimal(((BigInteger) arg0[1])))
                        .reduce(BigDecimal.ZERO, (a, b)
                                -> a.add(b)).divide(BigDecimal.valueOf(projectPerTask.isEmpty() ? 1L : (long) projectPerTask.size())))
                .cantProjectFinish(cantProjectFinish)
                .projectTimeLine(projectTimeLineList)
                .build();
    }
}
