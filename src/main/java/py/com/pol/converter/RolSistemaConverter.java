package py.com.pol.converter;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import py.com.pol.entity.RolesSistema;
import py.com.pol.model.request.RolSistemaRequest;
import py.com.pol.model.response.RolSistemaResponse;

/**
 *
 * @author Gino Junchaya
 */

@Component
public class RolSistemaConverter {
    
    public RolSistemaResponse entityToModel(RolesSistema entity){
        if(entity == null){ return null; }
        RolSistemaResponse model = new RolSistemaResponse();
        model.setId(entity.getIdRol());
        model.setDescripcion(entity.getDescripcion());
        model.setCodigo(entity.getCodigo());
        return model;
    }
    
    public List<RolSistemaResponse> entitiesToModels(List<RolesSistema> entities){
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        List<RolSistemaResponse> models = new ArrayList<>();
        entities.forEach((entity) -> {
            models.add(entityToModel(entity));
        });
        return models;
    }
    
    public RolesSistema modelToEntity(RolSistemaRequest model, Boolean insert){
        RolesSistema entity = new RolesSistema();
        entity.setDescripcion(model.getDescripcion());
        entity.setCodigo(model.getCodigo());
        if(!insert){
            entity.setIdRol(model.getId());
        }
        return entity;
    }    
    
}
