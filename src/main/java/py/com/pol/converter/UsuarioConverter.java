package py.com.pol.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import py.com.pol.entity.Usuario;
import py.com.pol.model.request.UsuarioRequest;
import py.com.pol.model.response.UserResponse;
import py.com.pol.model.response.UserResponseMin;

/**
 *
 * @author Gino Junchaya
 */

@Component
public class UsuarioConverter {
    
    @Autowired
    private RolSistemaConverter rolSistemaConverter;
    
    @Autowired
    private DatosPersonalesConverter datosPersonalesConverter;
    
    public List<UserResponse> entitiesToModels(List<Usuario> entities){
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        List<UserResponse> models = new ArrayList<>();
        entities.forEach((entity) -> {
            models.add(entityToModel(entity));
        });
        return models;
    }
    
    public UserResponseMin entityToModelMin(Usuario entity){
        if(entity == null)
            return null;
        UserResponseMin model = new UserResponseMin();
        model.setEmail(entity.getEmail());
        model.setUsername(entity.getUsername());
        model.setRol(rolSistemaConverter.entityToModel(entity.getIdRolSistema()));
        model.setDatosPersonales(datosPersonalesConverter.entityToModel(entity.getIdDatosPersonales()));
        model.setId(entity.getIdUsuario());
        model.setEstado(entity.getEstado());
        return model;
    }    
    
    public UserResponse entityToModel(Usuario entity){
        if(entity == null)
            return null;
        UserResponse model = new UserResponse();
        model.setAccessToken(entity.getAccessToken());
        model.setDeviceId(entity.getDeviceId());
        model.setEliminado(entity.getEliminado());
        model.setEmail(entity.getEmail());
        model.setUsername(entity.getUsername());
        model.setTokenPush(entity.getTokenPush());
        model.setRol(rolSistemaConverter.entityToModel(entity.getIdRolSistema()));
        model.setDatosPersonales(datosPersonalesConverter.entityToModel(entity.getIdDatosPersonales()));
        model.setId(entity.getIdUsuario());
        model.setFechaCreacion(entity.getFechaCreacion());
        model.setEstado(entity.getEstado());
        return model;
    }
    
    public Usuario buildEntity(UsuarioRequest request, Boolean insert){
        if(request == null){
            return null;
        }
        Usuario entity = new Usuario();
        entity.setEmail(request.getEmail());
        entity.setUsername(request.getUsername());
        entity.setFechaCreacion(new Date());
        if(!insert){
           entity.setIdUsuario(request.getId());
        }
        entity.setPassword("abcd.123");
        entity.setEliminado(Boolean.FALSE);
        entity.setEstado("A");
        return entity;
    }
    
}
