package py.com.pol.converter;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import py.com.pol.entity.DatosPersonales;
import py.com.pol.model.request.DatosPersonalesRequest;
import py.com.pol.model.response.DatosPersonalesResponse;

/**
 *
 * @author Gino Junchaya
 */

@Component
public class DatosPersonalesConverter {
    
    public DatosPersonalesResponse entityToModel(DatosPersonales entity){
        if(entity == null){ return null; }
        DatosPersonalesResponse model = new DatosPersonalesResponse();
        model.setApellidos(entity.getApellidos());
        model.setNombres(entity.getNombres());
        model.setFoto(entity.getFoto());
        model.setTelefono(entity.getTelefono());
        model.setId(entity.getId());
        model.setFechaNacimiento(entity.getFechaNacimiento());
        return model;
    }
    
    public List<DatosPersonalesResponse> entitiesToModels(List<DatosPersonales> entities){
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        List<DatosPersonalesResponse> models = new ArrayList<>();
        entities.forEach((entity) -> {
            models.add(entityToModel(entity));
        });
        return models;
    }
    
    public DatosPersonales modelToEntity(DatosPersonalesRequest model, Boolean insert){
        DatosPersonales entity = new DatosPersonales();
        entity.setApellidos(model.getApellidos());
        entity.setNombres(model.getNombres());
        entity.setFechaNacimiento(model.getFechaNacimiento());
        entity.setFoto(model.getFoto());
        entity.setTelefono(model.getTelefono());
        if(!insert){
            entity.setId(model.getId());
        }
        return entity;
    }
    
}
