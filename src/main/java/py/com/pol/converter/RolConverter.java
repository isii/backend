package py.com.pol.converter;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;
import py.com.pol.entity.Rol;
import py.com.pol.model.request.RolRequest;
import py.com.pol.model.response.RolResponse;

/**
 *
 * @author Gino Junchaya
 */

@Component
public class RolConverter {
    
    public RolResponse entityToModel(Rol entity){
        if(entity == null){ return null; }
        RolResponse model = new RolResponse();
        model.setId(entity.getIdRol());
        model.setDescripcion(entity.getDescripcion());
        model.setCodigo(entity.getCodigo());
        model.setNombre(entity.getNombre());
        return model;
    }
    
    public List<RolResponse> entitiesToModels(List<Rol> entities){
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        List<RolResponse> models = new ArrayList<>();
        entities.forEach((entity) -> {
            models.add(entityToModel(entity));
        });
        return models;
    }
    
    public Rol modelToEntity(RolRequest model, Boolean insert){
        Rol entity = new Rol();
        entity.setDescripcion(model.getDescripcion());
        entity.setNombre(model.getNombre());
        entity.setCodigo(model.getCodigo());
        if(!insert){
            entity.setIdRol(model.getId());
        }
        return entity;
    }
    
}
