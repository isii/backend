package py.com.pol.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import py.com.pol.entity.Items;
import py.com.pol.entity.Proyectos;
import py.com.pol.entity.TipoItem;
import py.com.pol.entity.Usuario;
import py.com.pol.model.request.ItemRequest;
import py.com.pol.model.response.ItemResponse;
import py.com.pol.util.enums.ItemStatus;

/**
 *
 * @author Gino Junchaya
 */

@Component
public class ItemsConverter {
    
    @Autowired
    private UsuarioConverter usuarioConverter;
    
    @Autowired
    private TipoItemConverter tipoItemConverter;
    
    public ItemResponse entityToModel(Items entity){
        if(entity == null){ return null; }
        ItemResponse model = new ItemResponse();
        model.setId(entity.getIdItem());
        model.setComplejidad(entity.getComplejidad());
        model.setVersion(entity.getVersion());
        model.setDescripcion(entity.getDescripcion());
        model.setEstado(entity.getEstado());
        model.setFechaCreacion(entity.getFechaCreacion());
        model.setTitulo(entity.getTitulo());
        model.setCreador(usuarioConverter.entityToModelMin(entity.getCreador()));
        model.setTipoItem(tipoItemConverter.entityToModel(entity.getIdTipoItem()));
        //model.setItems(items);
        return model;
    }
    
    public List<ItemResponse> entitiesToModels(List<Items> entities){
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        
        List<ItemResponse> models = new ArrayList<>();
        
        Optional<Items> first = searchFirst(entities);
        if(!first.isPresent())
            return new ArrayList<>();
        
        Optional<Items> next = Optional.empty();
        Items current = first.get();
        
        models.add(entityToModel(first.get()));
        
        do {
            next = searchNext(entities, current);
            if(next.isPresent()){
                models.add(entityToModel(next.get()));
                current = next.get();
            }
        }
        while(next.isPresent());
        
        return models;
    }
    
    public List<ItemResponse> entitiesToModelsAlter(List<Items> entities){
        if(entities == null || entities.isEmpty()){ return new ArrayList<>(); }
        List<ItemResponse> models = new ArrayList<>();
        entities.forEach(entity -> models.add(entityToModel(entity)));
        return models;
    }
    
    private Optional<Items> searchNext(List<Items> entities, Items current){
        return entities.stream().filter(item -> current.equals(item.getAntecesor())).findFirst();
    }
    
    private Optional<Items> searchFirst(List<Items> entities){
        return entities.stream().filter(item -> item.getAntecesor() == null).findFirst();
    }
    
    public List<Items> modelsToEntities(List<ItemRequest> models, Items padre, Usuario creador){
        if(models == null || models.isEmpty()){ return new ArrayList<>(); }
        List<Items> entities = new ArrayList<>();
        for(int i = 0; i < models.size(); i++){
            ItemRequest model = models.get(i);
            Items entity = modelToEntity(model, creador);
            entity.setPadre(padre);
            entities.add(entity);
            if(model.getItems() != null && !model.getItems().isEmpty()){
                entities.addAll(modelsToEntities(model.getItems(), entity, creador));
            }
        }
        return entities;
    }    
    
    public List<Items> modelsToEntities(List<ItemRequest> models, Usuario creador){
        if(models == null || models.isEmpty()){ return new ArrayList<>(); }
        List<Items> entities = new ArrayList<>();
        for(int i = 0; i < models.size(); i++){
            ItemRequest model = models.get(i);
            Items entity = modelToEntity(model, creador);
            entities.add(entity);
            if(model.getItems() != null && !model.getItems().isEmpty()){
                entities.addAll(modelsToEntities(model.getItems(), entity, creador));
            }
        }
        return entities;
    }
    
    public Items modelToEntity(ItemRequest model, Usuario creador){
        
        Items entity = new Items();
        entity.setDescripcion(model.getDescripcion());
        entity.setComplejidad(model.getComplejidad());
        entity.setEliminado(Boolean.FALSE);
        entity.setCreador(creador);
        entity.setVersion(1);
        entity.setTitulo(model.getTitulo());
        entity.setFechaCreacion(new Date());
        entity.setEstado(ItemStatus.PENDIENTE.getStatus());
        Proyectos proyecto = new Proyectos();
        proyecto.setIdProyecto(model.getProyecto());
        entity.setIdProyecto(proyecto);
        
        TipoItem tipoItem = new TipoItem();
        tipoItem.setIdTipoItem(model.getTipoItem());
        entity.setIdTipoItem(tipoItem);
        
        return entity;
    }
    
}
