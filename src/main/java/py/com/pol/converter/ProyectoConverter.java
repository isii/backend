package py.com.pol.converter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import py.com.pol.entity.Proyectos;
import py.com.pol.entity.Usuario;
import py.com.pol.model.request.ProyectoRequest;
import py.com.pol.model.response.ProyectoResponse;
import py.com.pol.util.enums.ProjectStatus;

/**
 *
 * @author Gino Junchaya
 */

@Component
public class ProyectoConverter {
    
    @Autowired
    private UsuarioConverter usuarioConverter;
    
    @Autowired
    private ItemsConverter itemsConverter;
    
    @Autowired
    private RolProyectoMemberConverter rolProyectoMemberConverter;
    
    public ProyectoResponse entityToModel(Proyectos entity){
        if(entity == null){ return null; }
        ProyectoResponse model = new ProyectoResponse();
        model.setId(entity.getIdProyecto());
        model.setNombre(entity.getNombre());
        model.setEstado(entity.getEstado());
        model.setDescripcion(entity.getDescripcion());
        model.setCreador(usuarioConverter.entityToModelMin(entity.getCreador()));
        model.setLider(usuarioConverter.entityToModelMin(entity.getLider()));        
        model.setFechaCreacion(entity.getFechaCreacion());
        model.setFechaFin(entity.getFechaFin());
        model.setFechaInicio(entity.getFechaInicio());
        model.setItems(itemsConverter.entitiesToModels(entity.getItemsList()));
        model.setMiembros(rolProyectoMemberConverter.entitiesToModels(entity.getRolesProyectoList()));
        return model;
    }
    
    public List<ProyectoResponse> entitiesToModels(List<Proyectos> entities){
        if(entities == null || entities.isEmpty()){
            return new ArrayList<>();
        }
        List<ProyectoResponse> models = new ArrayList<>();
        entities.forEach((entity) -> {
            models.add(entityToModel(entity));
        });
        return models;
    }
    
    public Proyectos modelToEntity(ProyectoRequest model, Usuario creador, Boolean insert){
        Proyectos entity = new Proyectos();
        entity.setDescripcion(model.getDescripcion());
        entity.setNombre(model.getNombre());
        entity.setEliminado(Boolean.FALSE);
        entity.setFechaCreacion(new Date());
        entity.setFechaInicio(model.getFechaInicio());
        entity.setFechaInicio(model.getFechaFin());        
        entity.setEstado(ProjectStatus.PENDIENTE.getStatus());
        entity.setCreador(creador);
        Usuario lider = new Usuario();
        lider.setIdUsuario(model.getLider());
        entity.setLider(lider);
        if(!insert){
            entity.setIdProyecto(model.getId());
        }
        return entity;
    }
    
}
