package py.com.pol.util.enums;

import java.util.Arrays;
import java.util.Optional;

public enum UserStatus {

    ACTIVO("A"),
    INACTIVO("I"),
    PENDIENTE("P");

    private final String desc;

    private UserStatus(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public static boolean getStatus(String desc) {
        Optional<UserStatus> rs = Arrays.asList(UserStatus.values()).stream().filter(f -> (f.getDesc() == null ? desc == null : f.getDesc().equals(desc))).findFirst();
        return rs.isPresent();
    }

    @Override
    public String toString() {
        return "UserStatus{" + "desc=" + desc + '}';
    }

}
