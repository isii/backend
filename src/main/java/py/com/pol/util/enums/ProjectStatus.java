package py.com.pol.util.enums;

public enum ProjectStatus {

    INICIADO("I"),
    PENDIENTE("P"),
    FINALIZADO("F");

    private final String status;

    private ProjectStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
