package py.com.pol.util.enums;

public enum ItemStatus {

    INICIADO("I"),
    PENDIENTE("P"),
    FINALIZADO("F");

    private final String status;

    private ItemStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
