package py.com.pol.util.enums;

/**
 *
 * @author Gino Junchaya
 */
public enum Direction {
    
    UP("UP"),
    DOWN("DOWN");
    
    private final String direction;
    
    Direction(String direction){
        this.direction = direction;
    }

    public String getDirection() {
        return direction;
    }
    
}
