package py.com.pol.util.enums;

public enum LineaBaseStatus {

    INICIADO("I"),
    PENDIENTE("P"),
    FINALIZADO("F");

    private final String status;

    private LineaBaseStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

}
