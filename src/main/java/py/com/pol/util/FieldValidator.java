package py.com.pol.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FieldValidator {

    private static final Logger LOGGER = LogManager.getLogger(FieldValidator.class);

    public static boolean emailValidator(String email) {
        Pattern pattern = Pattern
                .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        Matcher mather = pattern.matcher(email);
        if (mather.find() == true) {
            LOGGER.info("\nEl mail ingresado es válido!");
            return true;
        } else {
            LOGGER.info("\nEl email ingresado es inválido!");
            return false;
        }
    }

    public static String getMd5(String pwd) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(pwd.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public static byte[] stringToBase64(String image) {
        if(image == null){ return null; }
        Base64.Encoder enc = Base64.getEncoder();
        return enc.encode(image.getBytes());
    }
}
