
package py.com.pol.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Objects;

public class DateUtils {

    public static Date localDateAsDate(LocalDateTime localDateTime) {
        if(localDateTime == null){ return null; }
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static Date stringToDate(String value) throws ParseException {
        if(value == null){ return null; }
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.parse(value);
    }
    
    public static String datoToString(Date date){
        if(date == null){ return null; }
        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(date);
    }
    
    public static OffsetDateTime dateToOffsetDate(Date date) {
        return Objects.isNull(date) ? null : date.toInstant()
                                                 .atOffset(ZoneOffset.UTC);
    }

    public static Date localDateToDate(LocalDate localDate) {
        return Objects.isNull(localDate) ? null : Date.from(localDate
                                                            .atStartOfDay()
                                                            .atZone(ZoneId.systemDefault())
                                                            .toInstant());
    }
    
    public static LocalDate ofTimestampToString(Timestamp time) {
        return Objects.isNull(time) ? null : time.toLocalDateTime().toLocalDate();
    }
}
