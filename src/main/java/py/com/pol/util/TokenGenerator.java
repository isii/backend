package py.com.pol.util;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import java.time.LocalDateTime;
import java.util.HashMap;

public class TokenGenerator {

    public static String getToken(LocalDateTime dateIn, LocalDateTime dateExpire,
            String subject, String rol) {
        try {
            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.ES256;

            HashMap<String, Object> header = new HashMap<>();
            header.put("alg", signatureAlgorithm.toString());
            header.put("typ", "JWT");

            JwtBuilder tokenJWT = Jwts.builder()
                    .setHeader(header)
                    .setIssuedAt(DateUtils.localDateAsDate(dateIn))
                    .setExpiration(DateUtils.localDateAsDate(dateExpire))
                    .setSubject(subject)
                    .claim("scope", rol);

            return tokenJWT.compact();
        } catch (IllegalArgumentException e) {
            System.out.println(e);
            return "Error creating the token JWT" + e;
        }
    }
}
