package py.com.pol;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Is2BackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(Is2BackendApplication.class, args);
    }

}
