package py.com.pol.service;

import javax.servlet.http.HttpServletRequest;
import py.com.pol.entity.Usuario;
import py.com.pol.model.exception.ConverterException;
import py.com.pol.model.exception.DatabaseAccessException;
import py.com.pol.model.exception.InvalidCredentialsException;
import py.com.pol.model.request.LoginRequest;
import py.com.pol.model.request.RecoverConfirmRequest;
import py.com.pol.model.response.LoginResponse;

public interface AuthenticationService {

    public LoginResponse login(LoginRequest data, HttpServletRequest request) throws InvalidCredentialsException, ConverterException, DatabaseAccessException;
    public Usuario getByEmailAndAccessToken(RecoverConfirmRequest data, HttpServletRequest request);

}
