package py.com.pol.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.pol.converter.TipoItemConverter;
import py.com.pol.entity.TipoItem;
import py.com.pol.entity.Usuario;
import py.com.pol.model.exception.InvalidAccessTokenException;
import py.com.pol.model.request.TipoItemRequest;
import py.com.pol.model.response.TipoItemResponse;
import py.com.pol.repository.TipoItemRepository;
import py.com.pol.repository.UsuarioRepository;

/**
 *
 * @author Gino Junchaya
 */
@Service
public class TipoItemService {
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private TipoItemRepository tipoItemRepository;
    
    @Autowired
    private TipoItemConverter tipoItemConverter;
    
    public List<TipoItemResponse> findAll(String accessToken) throws InvalidAccessTokenException {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        return tipoItemConverter.entitiesToModels(
            (List<TipoItem>) tipoItemRepository.findAll()
        );
    }
    
    public TipoItemResponse save(TipoItemRequest request, String accessToken) throws InvalidAccessTokenException{
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        TipoItem entity = tipoItemConverter.modelToEntity(request, Boolean.TRUE);
        tipoItemRepository.save(entity);
        return tipoItemConverter.entityToModel(entity);
    }
    
    public TipoItemResponse update(TipoItemRequest request, String accessToken) throws InvalidAccessTokenException{
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }        
        Optional<TipoItem> tipoItemOp = tipoItemRepository.findById(request.getId());
        if(!tipoItemOp.isPresent()){
            throw new RuntimeException("El tipo de item no existe");
        }
        TipoItem tipoItem = tipoItemOp.get();
        tipoItem.setDescripcion(request.getDescripcion());
        tipoItemRepository.save(tipoItem);
        return tipoItemConverter.entityToModel(tipoItem);
        
    }
    
    public void delete(Integer id, String accessToken) throws InvalidAccessTokenException{
        
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }        
        Optional<TipoItem> tipoItemOp = tipoItemRepository.findById(id);
        if(!tipoItemOp.isPresent()){
            throw new RuntimeException("El tipo de item no existe");
        }
        TipoItem tipoItem = tipoItemOp.get();
        tipoItem.setEliminado(Boolean.TRUE);
        tipoItemRepository.save(tipoItem);
    }
    
}
