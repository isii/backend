package py.com.pol.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.pol.converter.ItemsConverter;
import py.com.pol.entity.Items;
import py.com.pol.entity.Usuario;
import py.com.pol.model.exception.InvalidAccessTokenException;
import py.com.pol.model.request.ItemRequest;
import py.com.pol.model.request.MoveRequest;
import py.com.pol.model.response.ItemResponse;
import py.com.pol.repository.ItemRepository;
import py.com.pol.repository.UsuarioRepository;
import py.com.pol.util.enums.Direction;

/**
 *
 * @author Gino Junchaya
 */
@Service
public class ItemService {
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private ItemRepository itemRepository;
    
    @Autowired
    private ItemsConverter itemsConverter;
    
    public List<ItemResponse> findAll(String accessToken, Integer projectId) throws InvalidAccessTokenException {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        return itemsConverter.entitiesToModels(itemRepository.findAllParents(projectId));
    }
    
    public List<ItemResponse> findAllTree(String accessToken, Integer projectId) throws InvalidAccessTokenException {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        List<ItemResponse> parents = itemsConverter.entitiesToModels(itemRepository.findAllParents(projectId));
        return recursiveSearchChildrens(parents);
    }    
    
    private List<ItemResponse> recursiveSearchChildrens(List<ItemResponse> parents){
        for(ItemResponse parent : parents){
            parent.setItems(findChildren(parent.getId()));
            recursiveSearchChildrens(parent.getItems());
        }
        return parents;
    }
    
    private List<ItemResponse> findChildren(Integer taskId){
        return itemsConverter.entitiesToModels(itemRepository.findChildrens(taskId));
    }    
    
    public List<ItemResponse> findChildren(String accessToken, Integer taskId) throws InvalidAccessTokenException {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        return itemsConverter.entitiesToModels(itemRepository.findChildrens(taskId));
    }
    
    public void addChild(String accessToken, Integer taskId, ItemRequest request) throws InvalidAccessTokenException{
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        Items item = itemRepository.findById(taskId).get();
        Items child = itemsConverter.modelToEntity(request, user);
        child.setRoot(Boolean.FALSE);
        child.setPadre(item);
        
        List<Items> currentItems = itemRepository.findChildrens(taskId);
        if(!currentItems.isEmpty()){
            Optional<Items> lastItemOp = findLast(currentItems);
            if(lastItemOp.isPresent()){
                Items lastItem = lastItemOp.get();
                child.setAntecesor(lastItem);
                child = itemRepository.save(child);
                lastItem.setSucesor(child);
                itemRepository.save(lastItem);                
            }
        }
        else {
            itemRepository.save(child);            
        }        
    }
    
    public ItemResponse findById(String accessToken, Integer taskId) throws InvalidAccessTokenException {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        return itemsConverter.entityToModel(itemRepository.findById(taskId).get());
    } 
    
    public void save(ItemRequest request, String accessToken) throws InvalidAccessTokenException{
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        Items entity = itemsConverter.modelToEntity(request, user);
        entity.setRoot(Boolean.TRUE);
        List<Items> currentItems = itemRepository.findAll(request.getProyecto());
        if(!currentItems.isEmpty()){
            Optional<Items> lastItemOp = findLast(currentItems);
            if(lastItemOp.isPresent()){
                Items lastItem = lastItemOp.get();
                entity.setAntecesor(lastItem);
                entity = itemRepository.save(entity);
                lastItem.setSucesor(entity);
                itemRepository.save(lastItem);                
            }
        }
        else {
            itemRepository.save(entity);            
        }
    }    
    
    private Optional<Items> findLast(List<Items> items){
        return items.stream().filter(item -> item.getSucesor() == null).findFirst();
    }
    
    public void saveAll(List<ItemRequest> request, String accessToken) throws InvalidAccessTokenException{
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        List<Items> entities = itemsConverter.modelsToEntities(request, user);
        if(entities != null && !entities.isEmpty()){
            itemRepository.saveAll(entities);
        }
    }
    
    public void delete(Integer item, String accessToken) throws InvalidAccessTokenException {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        Optional<Items> itemOp = itemRepository.findById(item);
        if(!itemOp.isPresent()){
            throw new RuntimeException("El item no existe");
        }
        Items itemObj = itemOp.get();
        itemObj.setEliminado(Boolean.TRUE);
        itemRepository.save(itemObj);
    }
    
    public ItemResponse update(ItemRequest request, String accessToken) throws InvalidAccessTokenException{
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        Optional<Items> itemOp = itemRepository.findById(request.getId());
        if(!itemOp.isPresent()){
            throw new RuntimeException("El item no existe");
        }
        Items itemObj = itemOp.get();
        itemObj.setTitulo(request.getTitulo());
        itemObj.setDescripcion(request.getDescripcion());
        itemObj.setComplejidad(request.getComplejidad());
        itemRepository.save(itemObj);        
        return itemsConverter.entityToModel(itemObj);
    }
    
    public void move(MoveRequest move, String accessToken) throws InvalidAccessTokenException {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        Optional<Items> itemOp = itemRepository.findById(move.getTaskId());
        if(!itemOp.isPresent()){
            throw new RuntimeException("El item no existe");
        }
        Items item = itemOp.get();
        
//        [8: ]       [8: ]
//        [5: ]       [1:X]
//        [1:X]       [5: ]
//                    
//        soy el antecesor de mi antecesor
//        soy el sucesor del antecesor de mi antecesor
//        mi antecesor es mi sucesor
//        el antecesor de mi antecesor es mi antecesor
        
        if(
            Direction.UP.getDirection().equals(move.getDirection()) && 
            inInferiorLimit(item)
        )
        {
            Items antecesor = item.getAntecesor();
            antecesor.setSucesor(null);
            Items antecesorAntecesor = antecesor.getAntecesor();

            antecesor.setAntecesor(item);            
            item.setSucesor(antecesor);
            item.setAntecesor(antecesorAntecesor);
            
            if(antecesorAntecesor != null){
                antecesorAntecesor.setSucesor(item);
                itemRepository.save(antecesorAntecesor);
            }
            
            itemRepository.save(item);
            itemRepository.save(antecesor);
            
        }
        
//        [8: ]       [8: ]
//        [5: ]       [1:X]
//        [1:X]       [5: ]
//        [4: ]       [4: ]
//                    
//        soy el antecesor de mi antecesor
//        soy el sucesor del antecesor de mi antecesor
//        mi antecesor es mi sucesor
//        mi antecesor es el antecesor de mi sucesor
//        mi sucesor es el sucesor de mi antecesor
//        el antecesor de mi antecesor es mi antecesor          
        
        else if(
            Direction.UP.getDirection().equals(move.getDirection()) && 
            !inInferiorLimit(item)
        )
        {
            
            Items antecesor = item.getAntecesor();
            antecesor.setSucesor(null);
            Items antecesorAntecesor = antecesor.getAntecesor();
            
            Items sucesor = item.getSucesor();
            sucesor.setAntecesor(antecesor);
            
            antecesor.setSucesor(sucesor);
            sucesor.setAntecesor(antecesor);
            
            antecesor.setAntecesor(item);            
            item.setSucesor(antecesor);
            item.setAntecesor(antecesorAntecesor);
            
            
            
            if(antecesorAntecesor != null){
                antecesorAntecesor.setSucesor(item);
                itemRepository.save(antecesorAntecesor);
            }
            
            itemRepository.save(item);
            itemRepository.save(antecesor);                
            itemRepository.save(sucesor);
            
        }
        
        //        [2:X]       [5: ]
        //        [5: ]       [2:X]
        //        [1: ]       [1: ]
        //        [4: ]       [4: ]
        //                    
        //        mi sucesor se convierte en mi antecesor
        //        el sucesor de mi sucesor se convierte en mi sucesor
        //        soy el sucesor de mi sucesor            
        //        soy el antecesor del sucesor de mi sucesor
        
        else if(
            Direction.DOWN.getDirection().equals(move.getDirection()) && 
            inSuperiorLimit(item)
        )
        {
            
            Items sucesor = item.getSucesor();
            sucesor.setAntecesor(null);
            Items sucesorSucesor = sucesor.getSucesor();
            
            item.setAntecesor(sucesor);
            item.setSucesor(sucesorSucesor);
            
            sucesor.setSucesor(item);
            
            if(sucesorSucesor != null){
                sucesorSucesor.setAntecesor(item);
                itemRepository.save(sucesorSucesor);
            }
            
            itemRepository.save(sucesor);
            itemRepository.save(item);
            
        }
        
        //        [4: ]       [4: ]            
        //        [2:X]       [5: ]
        //        [5: ]       [2:X]
        //        [1: ]       [1: ]
        //                    
        //        mi sucesor se convierte en mi antecesor
        //        el sucesor de mi sucesor se convierte en mi sucesor
        //        soy el sucesor de mi sucesor            
        //        soy el antecesor del sucesor de mi sucesor
        //        mi sucesor se convierte en el sucesor de mi antecesor
        //        mi antecesor se convierte en el antecesor de mi sucesor        
        
        else if(
            Direction.DOWN.getDirection().equals(move.getDirection()) && 
            !inSuperiorLimit(item)
        )
        {
            
            Items sucesor = item.getSucesor();
            Items sucesorSucesor = sucesor.getSucesor();
            
            Items antecesor = item.getAntecesor();
            antecesor.setSucesor(sucesor);
            sucesor.setAntecesor(antecesor);
            
            item.setAntecesor(sucesor);
            item.setSucesor(sucesorSucesor);
            
            sucesor.setSucesor(item);
            
            if(sucesorSucesor != null){
                sucesorSucesor.setAntecesor(item);
                itemRepository.save(sucesorSucesor);
            }
            
            itemRepository.save(antecesor);
            itemRepository.save(sucesor);
            itemRepository.save(item);            
            
        }        
    }
    
    private Boolean inInferiorLimit(Items item){
        return item.getSucesor() == null;
    }
    
    private Boolean inSuperiorLimit(Items item){
        return item.getAntecesor() == null;
    }
    
}
