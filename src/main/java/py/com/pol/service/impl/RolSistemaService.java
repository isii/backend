package py.com.pol.service.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.pol.converter.RolSistemaConverter;
import py.com.pol.entity.RolesSistema;
import py.com.pol.model.request.RolSistemaRequest;
import py.com.pol.model.response.RolSistemaResponse;
import py.com.pol.repository.RolSistemaRepository;

/**
 *
 * @author Gino Junchaya
 */

@Service
public class RolSistemaService {
    
    @Autowired
    private RolSistemaRepository rolRepository;
    
    @Autowired
    private RolSistemaConverter rolSistemaConverter;
    
    public List<RolSistemaResponse> findAll() {
        return rolSistemaConverter
        .entitiesToModels((List<RolesSistema>) rolRepository.findAll());
    }
    
    public RolSistemaResponse save(RolSistemaRequest request){
        RolesSistema entity = rolSistemaConverter.modelToEntity(request, Boolean.TRUE);
        rolRepository.save(entity);
        return rolSistemaConverter.entityToModel(entity);
    }
    
    public RolSistemaResponse update(RolSistemaRequest request){
        RolesSistema entity = rolSistemaConverter.modelToEntity(request, Boolean.FALSE);
        rolRepository.save(entity);
        return rolSistemaConverter.entityToModel(entity);
    }
    
    public void delete(Integer id){
        Optional<RolesSistema> entity = rolRepository.findById(id);
        if(!entity.isPresent()){
            throw new RuntimeException("El rol no existe");
        }
        rolRepository.save(entity.get());
    }    
    
}
