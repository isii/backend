package py.com.pol.service.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.pol.converter.DatosPersonalesConverter;
import py.com.pol.converter.UsuarioConverter;
import py.com.pol.entity.DatosPersonales;
import py.com.pol.entity.RolesSistema;
import py.com.pol.entity.Usuario;
import py.com.pol.model.request.UsuarioRequest;
import py.com.pol.model.response.UserResponse;
import py.com.pol.repository.DatosPersonalesRepository;
import py.com.pol.repository.RolSistemaRepository;
import py.com.pol.repository.UsuarioRepository;

/**
 *
 * @author Gino Junchaya
 */
@Service
public class UsuarioService {
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private RolSistemaRepository rolSistemaRepository;
    
    @Autowired
    private DatosPersonalesRepository datosPersonalesRepository;
    
    @Autowired
    private UsuarioConverter usuarioConverter;
    
    @Autowired
    private DatosPersonalesConverter datosPersonalesConverter;
    
    public List<UserResponse> findAll() {
        return usuarioConverter.entitiesToModels(usuarioRepository.findAll());
    }
    
    public UserResponse save(UsuarioRequest request){
        
        DatosPersonales datosPersonales = datosPersonalesConverter.modelToEntity(request.getDatosPersonales(), Boolean.TRUE);
        datosPersonalesRepository.save(datosPersonales);
        
        Optional<RolesSistema> rol = rolSistemaRepository.findById(request.getRolSistema());
        if(!rol.isPresent()){
            throw new RuntimeException("El rol de sistema no existe");
        }
        
        Usuario entity = usuarioConverter.buildEntity(request, Boolean.TRUE);
        entity.setIdDatosPersonales(datosPersonales);
        entity.setIdRolSistema(rol.get());
        
        usuarioRepository.save(entity);
        
        return usuarioConverter.entityToModel(entity);        
    }
    
    public UserResponse update(UsuarioRequest request){
        
        Optional<Usuario> userOp = usuarioRepository.findById(request.getId());
        if(!userOp.isPresent()){
            throw new RuntimeException("El usuario no existe");
        }
        Usuario user = userOp.get();
        
        RolesSistema nuevoRol = new RolesSistema();
        nuevoRol.setIdRol(request.getRolSistema());
        
        user.setIdRolSistema(nuevoRol);
        
        usuarioRepository.save(user);
        
        if(request.getDatosPersonales() != null){
            
            DatosPersonales datosPersonales = user.getIdDatosPersonales();
            datosPersonales.setApellidos(request.getDatosPersonales().getApellidos());
            datosPersonales.setNombres(request.getDatosPersonales().getNombres());
            datosPersonales.setFoto(request.getDatosPersonales().getFoto());
            datosPersonales.setTelefono(request.getDatosPersonales().getTelefono());
            
            datosPersonalesRepository.save(datosPersonales);
            user.setIdDatosPersonales(datosPersonales);
        }
        
        return usuarioConverter.entityToModel(user);
        
    }
    
    public void delete(Integer id){
        Optional<Usuario> userOp = usuarioRepository.findById(id);
        if(!userOp.isPresent()){
            throw new RuntimeException("El usuario no existe");
        }
        Usuario user = userOp.get();
        user.setEliminado(Boolean.TRUE);
        usuarioRepository.save(user);
    }
    
}
