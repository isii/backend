package py.com.pol.service.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.pol.converter.RolProyectoMemberConverter;
import py.com.pol.entity.Proyectos;
import py.com.pol.entity.Rol;
import py.com.pol.entity.RolesProyecto;
import py.com.pol.entity.Usuario;
import py.com.pol.model.exception.InvalidAccessTokenException;
import py.com.pol.model.request.RolesProyectoMiembrosRequest;
import py.com.pol.model.response.RolesProyectoMiembros;
import py.com.pol.repository.ProyectoRepository;
import py.com.pol.repository.RolesProyectoRepository;
import py.com.pol.repository.UsuarioRepository;

/**
 *
 * @author Gino Junchaya
 */
@Service
public class RolesProyectoMemberService {
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private ProyectoRepository proyectoRepository;
    
    @Autowired
    private RolesProyectoRepository rolesProyectoRepository;
    
    @Autowired
    private RolProyectoMemberConverter rolProyectoMemberConverter;
    
    public List<RolesProyectoMiembros> findAll(String accessToken, Integer projectId) throws InvalidAccessTokenException {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        return rolProyectoMemberConverter.entitiesToModels(
            rolesProyectoRepository.findAll(projectId)
        );
    }
    
    public RolesProyectoMiembros save(RolesProyectoMiembrosRequest request, String accessToken) throws InvalidAccessTokenException{
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        Optional<Proyectos> proyectoOp = proyectoRepository.findById(request.getProyecto());
        if(!proyectoOp.isPresent()){
            throw new RuntimeException("El proyecto no existe");
        }
        if(!proyectoOp.get().getCreador().getIdUsuario().equals(user.getIdUsuario())){
            throw new RuntimeException("Solo el owner puede agregar miembros");
        }
        Proyectos proyecto = proyectoOp.get();
        if(proyecto.getRolesProyectoList() != null){
            for(RolesProyecto rolProyecto : proyecto.getRolesProyectoList()){
                if(rolProyecto.getIdUsuario().getIdUsuario().equals(request.getMiembro())){
                    if(!rolProyecto.getEliminado())
                        throw new RuntimeException("El miembro ya existe");
                }
            }
        }
        RolesProyecto entity = rolProyectoMemberConverter.modelToEntity(request, user, Boolean.TRUE);
        rolesProyectoRepository.save(entity);
        return rolProyectoMemberConverter.entityToModel(entity);
    }
    
    public RolesProyectoMiembros update(RolesProyectoMiembrosRequest request, String accessToken) throws InvalidAccessTokenException{
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        Optional<RolesProyecto> miembroOp = rolesProyectoRepository.findById(request.getId());
        if(!miembroOp.isPresent()){
            throw new RuntimeException("El miembro no existe");
        }
        RolesProyecto miembro = miembroOp.get();
        Rol rolNuevo = new Rol();
        rolNuevo.setIdRol(request.getRol());
        miembro.setRol(rolNuevo);
        
        rolesProyectoRepository.save(miembro);
        return rolProyectoMemberConverter.entityToModel(miembro);
    }
    
    public void delete(Integer id, String accessToken) throws InvalidAccessTokenException{
        
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }       
        Optional<RolesProyecto> miembroOp = rolesProyectoRepository.findById(id);
        if(!miembroOp.isPresent()){
            throw new RuntimeException("El miembro no existe");
        }
        RolesProyecto miembro = miembroOp.get();
        miembro.setEliminado(Boolean.TRUE);
        rolesProyectoRepository.save(miembro);
    }
    
}
