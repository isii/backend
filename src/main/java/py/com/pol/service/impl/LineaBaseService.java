package py.com.pol.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.pol.converter.LineaBaseConverter;
import py.com.pol.entity.Items;
import py.com.pol.entity.LineaBase;
import py.com.pol.entity.Usuario;
import py.com.pol.model.exception.InvalidAccessTokenException;
import py.com.pol.model.request.LineaBaseRequest;
import py.com.pol.model.response.LineaBaseResponse;
import py.com.pol.repository.ItemRepository;
import py.com.pol.repository.LineaBaseRepository;
import py.com.pol.repository.UsuarioRepository;

/**
 *
 * @author Gino Junchaya
 */
@Service
public class LineaBaseService {
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private LineaBaseRepository lineaBaseRepository;
    
    @Autowired
    private ItemRepository itemRepository;
    
    @Autowired
    private LineaBaseConverter lineaBaseConverter;
    
    public List<LineaBaseResponse> findAll(
        String accessToken, Integer projectId
    )
    throws 
    InvalidAccessTokenException 
    {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        return lineaBaseConverter.entitiesToModels(
            lineaBaseRepository.findAll(projectId)
        );
    }
        
    public void save(LineaBaseRequest request, String accessToken) throws InvalidAccessTokenException{
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        List<Items> items = validateItems(request.getItems());
        if(items.isEmpty()){
            throw new RuntimeException("Se deben seleccionar tareas para la línea base");
        }
        LineaBase entity = lineaBaseConverter.buildEntity(request, items, user);
        lineaBaseRepository.save(entity);
    }    
    
    private List<Items> validateItems(List<Integer> itemsList){
        if(itemsList == null || itemsList.isEmpty()){
            return new ArrayList<>();
        }
        List<Items> items = new ArrayList<>();
        itemsList.stream()
        .forEach(
            item -> items.add(
                itemRepository.findById(item).get()
            )
        );
        return items;
    }
    
}
 