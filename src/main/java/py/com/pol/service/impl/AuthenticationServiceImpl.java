package py.com.pol.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.pol.entity.Usuario;
import py.com.pol.model.request.LoginRequest;
import py.com.pol.model.request.RecoverConfirmRequest;
import py.com.pol.repository.UsuarioRepository;
import java.time.LocalDateTime;
import javax.servlet.http.HttpServletRequest;
import py.com.pol.model.exception.ConverterException;
import py.com.pol.converter.AuthenticationConverter;
import py.com.pol.model.exception.DatabaseAccessException;
import py.com.pol.model.exception.InvalidCredentialsException;
import py.com.pol.model.response.LoginResponse;
import py.com.pol.service.AuthenticationService;
import py.com.pol.util.TokenGenerator;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    
    private static final Logger LOGGER = LogManager.getLogger(AuthenticationServiceImpl.class);
    
    @Autowired
    private UsuarioRepository repositoryAuthentication;
    
    @Autowired
    private AuthenticationConverter converter;

    @Override
    public LoginResponse login(LoginRequest data, HttpServletRequest request) throws InvalidCredentialsException, ConverterException, DatabaseAccessException {
        LOGGER.info("Buscando Datos del Usuario > " + data.getEmail());
        Usuario usuario = repositoryAuthentication.findByEmailPassword(data.getEmail(), data.getPassword());
        if(usuario == null) {
            usuario = repositoryAuthentication.findByUsernamePassword(data.getEmail(), data.getPassword());
            if(usuario == null)
                throw new InvalidCredentialsException("Correo electrónico o contraseña incorrectos");
        }
        LocalDateTime dateIn = LocalDateTime.now();
        LocalDateTime dateExpire = LocalDateTime.now().plusMinutes(30L);
        LOGGER.info("Generando token de acceso > " + data.getEmail());
        String token = TokenGenerator.getToken(dateIn, dateExpire, data.getEmail(), usuario.getIdRolSistema().getCodigo());
        LOGGER.info("Actualizando token de acceso > " + data.getEmail());
        usuario.setAccessToken(token);
        LOGGER.info("Actualizando device > " + request.getRemoteAddr());
        usuario.setDeviceId(request.getRemoteAddr());
        repositoryAuthentication.save(usuario);
        return converter.buildEntityToModel(usuario);
    }

    @Override
    public Usuario getByEmailAndAccessToken(RecoverConfirmRequest data, HttpServletRequest request) {
        try {
            LOGGER.info("Buscando Datos del Usuario > " + data.getEmail() + ":" + data.getAccessToken() );
            Usuario usuario = repositoryAuthentication.findByEmailAccessToken(data.getEmail(), data.getAccessToken());
            if (usuario != null) {
                LOGGER.info("Actualizando Datos del usuario > " + data.getEmail());
                usuario.setPassword(data.getNewPassword());
                usuario.setAccessToken(null);
                usuario.setDeviceId(null);
                repositoryAuthentication.save(usuario);
                return usuario;
            } else {
                return null;
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return null;
        }
    }

}