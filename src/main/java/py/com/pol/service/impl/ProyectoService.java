package py.com.pol.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.pol.converter.ProyectoConverter;
import py.com.pol.entity.Proyectos;
import py.com.pol.entity.RolesProyecto;
import py.com.pol.entity.Usuario;
import py.com.pol.model.exception.InvalidAccessTokenException;
import py.com.pol.model.request.ProyectoRequest;
import py.com.pol.model.response.ProyectoResponse;
import py.com.pol.repository.ProyectoRepository;
import py.com.pol.repository.RolRepository;
import py.com.pol.repository.RolesProyectoRepository;
import py.com.pol.repository.UsuarioRepository;

/**
 *
 * @author Gino Junchaya
 */
@Service
public class ProyectoService {
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private ProyectoRepository proyectoRepository;
    
    @Autowired
    private RolesProyectoRepository rolesProyectoRepository;
    
    @Autowired
    private RolRepository rolRepository;    
    
    @Autowired
    private ProyectoConverter proyectoConverter;
    
    public List<ProyectoResponse> findAll(String accessToken) throws InvalidAccessTokenException {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        return proyectoConverter.entitiesToModels(
            proyectoRepository.findAll(user.getIdUsuario())
        );
    }
    
    public ProyectoResponse findById(Integer id, String accessToken) throws InvalidAccessTokenException {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        Optional<Proyectos> proyectoOp = proyectoRepository.findById(id);
        if(!proyectoOp.isPresent()){
            return null;
        }
        return proyectoConverter.entityToModel(proyectoOp.get());
    }
    
    public ProyectoResponse save(ProyectoRequest request, String accessToken) throws InvalidAccessTokenException{
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        
        Proyectos entity = proyectoConverter.modelToEntity(request, user, Boolean.TRUE);
        proyectoRepository.save(entity);
        
        List<RolesProyecto> roles = new ArrayList<>();

        RolesProyecto miembro = new RolesProyecto();

        miembro.setAutorizador(user);
        miembro.setIdUsuario(user);
        miembro.setEliminado(Boolean.FALSE);
        miembro.setFechaCreacion(new Date());
        miembro.setRol(rolRepository.findByCode("OWN"));
        miembro.setProyectos(entity);
        
        rolesProyectoRepository.save(miembro);

        roles.add(miembro);
        entity.setRolesProyectoList(roles);
        
        proyectoRepository.save(entity);
        
        return proyectoConverter.entityToModel(entity);
        
    }
    
    public ProyectoResponse update(ProyectoRequest request, String accessToken) throws InvalidAccessTokenException{
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }        
        Optional<Proyectos> proyectoOp = proyectoRepository.findById(request.getId());
        if(!proyectoOp.isPresent()){
            throw new RuntimeException("El proyecto no existe");
        }
        Proyectos proyecto = proyectoOp.get();
        proyecto.setNombre(request.getNombre());
        proyecto.setDescripcion(request.getDescripcion());
        proyecto.setFechaInicio(request.getFechaInicio());
        proyecto.setFechaFin(request.getFechaFin());
        
        proyectoRepository.save(proyecto);
        return proyectoConverter.entityToModel(proyecto);
        
    }
    
    public void delete(Integer id, String accessToken) throws InvalidAccessTokenException{
        
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }        
        Optional<Proyectos> proyectoOp = proyectoRepository.findById(id);
        if(!proyectoOp.isPresent()){
            throw new RuntimeException("El proyecto no existe");
        }
        Proyectos proyecto = proyectoOp.get();
        proyecto.setEliminado(Boolean.TRUE);
        proyectoRepository.save(proyecto);
    }
    
}
