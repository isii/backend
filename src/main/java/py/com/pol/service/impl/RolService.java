package py.com.pol.service.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.pol.converter.RolConverter;
import py.com.pol.entity.Rol;
import py.com.pol.model.request.RolRequest;
import py.com.pol.model.response.RolResponse;
import py.com.pol.repository.RolRepository;

/**
 *
 * @author Gino Junchaya
 */
@Service
public class RolService {
    
    @Autowired
    private RolRepository rolRepository;
    
    @Autowired
    private RolConverter rolConverter;
    
    public List<RolResponse> findAll() {
        return rolConverter.entitiesToModels((List<Rol>) rolRepository.findAll());
    }
    
    public RolResponse save(RolRequest request){
        Rol entity = rolConverter.modelToEntity(request, Boolean.TRUE);
        rolRepository.save(entity);
        return rolConverter.entityToModel(entity);
    }
    
    public RolResponse update(RolRequest request){
        Rol entity = rolConverter.modelToEntity(request, Boolean.FALSE);
        rolRepository.save(entity);
        return rolConverter.entityToModel(entity);
    }
    
    public void delete(Integer id){
        Optional<Rol> entity = rolRepository.findById(id);
        if(!entity.isPresent()){
            throw new RuntimeException("El rol no existe");
        }
        rolRepository.save(entity.get());
    }
    
}
