package py.com.pol.service.impl;

import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import py.com.pol.converter.StatisticsConverter;
import py.com.pol.entity.Usuario;
import py.com.pol.model.exception.InvalidAccessTokenException;
import py.com.pol.model.response.Statistics;
import py.com.pol.repository.ProyectoRepository;
import py.com.pol.repository.UsuarioRepository;

@Service
public class StatisticsService {
    
    @Autowired
    private ProyectoRepository proyectoRepository;
    
    @Autowired
    private UsuarioRepository usuarioRepository;
    
    @Autowired
    private StatisticsConverter statisticsConverter;

    public Statistics getStatistics(String accessToken) throws InvalidAccessTokenException {
        Usuario user = usuarioRepository.findByAccessToken(accessToken);
        if (Objects.isNull(user)) {
            throw new InvalidAccessTokenException("Invalid access token");
        }
        Long cantUsr = usuarioRepository.getQuantityUsers();
        List<Object[]> percentUsrRol = usuarioRepository.percentageOfUsersByRole();
        Long cantProject = proyectoRepository.getQuantityProjects();
        List<Object[]> projectPerTask = proyectoRepository.numberOfTasksPerProject();
        Long cantProjectFinish = proyectoRepository.numberOfCompletedProjects();
        List<Object[]> projectTimeLine = proyectoRepository.getProjectsTimeLine();
        return statisticsConverter.buildStatistics(cantUsr, percentUsrRol,
            cantProject, projectPerTask, cantProjectFinish, projectTimeLine);
    }
}
