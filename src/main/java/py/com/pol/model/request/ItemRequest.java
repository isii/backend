package py.com.pol.model.request;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gino Junchaya
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemRequest {
    
    private Integer id;
    private String titulo;
    private String descripcion;    
    private Integer complejidad;
    private Integer proyecto;
    private Integer tipoItem;
    private List<ItemRequest> items;

}
