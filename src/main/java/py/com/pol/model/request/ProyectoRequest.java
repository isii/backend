package py.com.pol.model.request;

import java.util.Date;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gino Junchaya
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProyectoRequest {
    
    private Integer id;
    
    @NotNull
    private String nombre;
    
    private String descripcion;
    
    @NotNull
    private Integer lider;
    
    private Date fechaInicio;
    private Date fechaFin;    

}
