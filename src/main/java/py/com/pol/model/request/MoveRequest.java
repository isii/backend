package py.com.pol.model.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gino Junchaya
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoveRequest {
    
    @NotNull
    private Integer taskId;
    
    @Pattern(regexp = "UP|DOWN")
    private String direction;
    
}
