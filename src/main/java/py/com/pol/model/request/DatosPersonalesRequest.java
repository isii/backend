package py.com.pol.model.request;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gino Junchaya
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatosPersonalesRequest {
    
    private Integer id;
    private String nombres;
    private String apellidos;
    private String telefono;
    private String foto;
    private Date fechaNacimiento;    
    
}
