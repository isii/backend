package py.com.pol.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gino Junchaya
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UsuarioRequest {
    
    private Integer id;
    private String email;
    private String username;
    private DatosPersonalesRequest datosPersonales;
    private Integer rolSistema;
    
}
