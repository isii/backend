package py.com.pol.model.request;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolesProyectoMiembrosRequest implements Serializable {

    private Integer id;
    private Integer rol;
    private Integer miembro;
    private Integer proyecto;

}
