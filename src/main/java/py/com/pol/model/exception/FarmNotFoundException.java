package py.com.pol.model.exception;

public class FarmNotFoundException extends Exception {

    public FarmNotFoundException(String message){
        super(message);
    }

}
