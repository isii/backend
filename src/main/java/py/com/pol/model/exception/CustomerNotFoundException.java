package py.com.pol.model.exception;

public class CustomerNotFoundException extends Exception {

    public CustomerNotFoundException(String message){
        super(message);
    }

}
