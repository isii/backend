package py.com.pol.model.exception;

public class RecoverRequestException extends Exception {

    public RecoverRequestException(String message){
        super(message);
    }

}
