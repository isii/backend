package py.com.pol.model.exception;

public class ConverterException extends Exception {

    public ConverterException(String message){
        super(message);
    }

}
