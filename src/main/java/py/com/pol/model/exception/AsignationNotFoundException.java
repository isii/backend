package py.com.pol.model.exception;

public class AsignationNotFoundException extends Exception {

    public AsignationNotFoundException(String message){
        super(message);
    }

}
