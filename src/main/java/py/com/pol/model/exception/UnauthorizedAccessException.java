package py.com.pol.model.exception;

public class UnauthorizedAccessException extends Exception {

    public UnauthorizedAccessException(String message){
        super(message);
    }

}
