package py.com.pol.model.exception;

public class DatabaseAccessException extends Exception {

    public DatabaseAccessException(String message){
        super(message);
    }

}
