package py.com.pol.model.exception;

public class ReportCreationException extends Exception {

    public ReportCreationException(String message){
        super(message);
    }

}
