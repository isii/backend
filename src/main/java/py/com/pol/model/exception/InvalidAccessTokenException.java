package py.com.pol.model.exception;

public class InvalidAccessTokenException extends Exception {

    public InvalidAccessTokenException(String message){
        super(message);
    }

}
