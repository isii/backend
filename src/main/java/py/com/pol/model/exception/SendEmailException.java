package py.com.pol.model.exception;

public class SendEmailException extends Exception {

    public SendEmailException(String message){
        super(message);
    }

}
