package py.com.pol.model.exception;

public class InvalidCredentialsException extends Exception {

    public InvalidCredentialsException(String message){
        super(message);
    }

}
