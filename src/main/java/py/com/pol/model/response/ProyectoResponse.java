package py.com.pol.model.response;

import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gino Junchaya
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProyectoResponse {
    
    private Integer id;
    private String nombre;
    private String descripcion;    
    private Date fechaCreacion;
    private String estado;
    private Date fechaFin;
    private Date fechaInicio;
    private List<RolesProyectoMiembros> miembros;
    private UserResponseMin creador;
    private UserResponseMin lider;    
    private List<ItemResponse> items;
    
}
