package py.com.pol.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoginResponse {

    @JsonProperty(value = "id")
    private Integer id;
    
    @JsonProperty(value = "email")
    private String email;
    
    @JsonProperty(value = "access_token")
    private String accessToken;
    
    @JsonProperty(value = "rol")
    private String rol;
    
    @JsonProperty(value = "age")
    private String age;

    @JsonProperty("foto")
    private String photo;

    @JsonProperty("username")    
    private String username;
}
