package py.com.pol.model.response;

import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gino Junchaya
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemResponse {
    
    private Integer id;
    private String titulo;
    private String descripcion;    
    private Date fechaCreacion;
    private String estado;
    private Integer complejidad;
    private Integer version;
    private UserResponseMin creador;
    private List<ItemResponse> items;
    private TipoItemResponse tipoItem;    
    
}
