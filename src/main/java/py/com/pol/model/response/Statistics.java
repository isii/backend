package py.com.pol.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Statistics implements Serializable {
    
    @JsonProperty("cantidad_usuarios")
    private Long cantUsr;

    @JsonProperty("porcentaje_usuario_rol")
    private Map<String, Object> mapUser;
    
    @JsonProperty("cantidad_proyectos")
    private Long cantProject;
    
    @JsonSerialize(as = BigDecimal.class)
    @JsonProperty("promedio_tareas_proyecto")
    private Number averageTask;
    
    @JsonProperty("cantidad_proyectos_terminados")
    private Long cantProjectFinish;
    
    @JsonProperty("linea_tiempo_proyecto")
    private List<TimeLineGraphing> projectTimeLine;
}
