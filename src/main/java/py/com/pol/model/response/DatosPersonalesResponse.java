package py.com.pol.model.response;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatosPersonalesResponse implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Integer id;
    private String nombres;
    private String apellidos;
    private String telefono;
    private String foto;
    private Date fechaNacimiento;
    
}
