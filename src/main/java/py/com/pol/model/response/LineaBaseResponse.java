package py.com.pol.model.response;

import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gino Junchaya
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LineaBaseResponse {
    
    private Integer id;
    private Date fechaCreacion;
    private String estado;
    private UserResponseMin creador;
    private List<ItemResponse> items;
    private ProyectoResponse proyecto;

}