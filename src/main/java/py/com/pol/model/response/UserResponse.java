package py.com.pol.model.response;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gino Junchaya
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    
    private Integer id;
    private String email;
    private String estado;
    private String username;    
    private String tokenPush;
    private Boolean eliminado;
    private Date fechaCreacion;
    private String accessToken;
    private String deviceId;
    private DatosPersonalesResponse datosPersonales;
    private RolSistemaResponse rol;
    
}
