package py.com.pol.model.response;

import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolesProyectoMiembros implements Serializable {
    
    private Integer id;
    private RolResponse rol;
    private Date fechaCreacion;
    private UserResponseMin miembro;
    private UserResponseMin autorizador;

}
