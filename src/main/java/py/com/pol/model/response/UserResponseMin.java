package py.com.pol.model.response;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gino Junchaya
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseMin {
    
    private Integer id;
    private String email;
    private String estado;
    private String username;
    private DatosPersonalesResponse datosPersonales;
    private RolSistemaResponse rol;
    
}
