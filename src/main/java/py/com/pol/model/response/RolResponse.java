package py.com.pol.model.response;

import py.com.pol.model.request.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Gino Junchaya
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RolResponse {
    
    @JsonProperty("id")
    private Integer id;
    
    @JsonProperty("descripcion")
    private String descripcion;
    
    @JsonProperty("nombre")
    private String nombre;    
    
    @JsonProperty("codigo")
    private String codigo;    
    
}
