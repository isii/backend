package py.com.pol.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import py.com.pol.model.error.ErrorData;
import py.com.pol.model.error.ResourceError;
import py.com.pol.model.request.TipoItemRequest;
import py.com.pol.model.response.TipoItemResponse;
import py.com.pol.service.impl.TipoItemService;

/**
 *
 * @author Gino Junchaya
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/type-items")
public class TipoItemController {
    
    private static final Logger LOGGER = LogManager.getLogger(TipoItemController.class);    
 
    @Autowired
    private TipoItemService tipoItemService;
    
    @PostMapping
    public ResponseEntity<?> save(
        @Valid @RequestBody TipoItemRequest body, 
        @RequestHeader("access_token") String accessToken,
        HttpServletRequest request
    )
    {
        try {
            LOGGER.info("Calling tipoItemService.save(?, ?)", body, accessToken);
            tipoItemService.save(body, accessToken);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }        
    }
    
    @PutMapping
    public ResponseEntity<?> update(
        @Valid @RequestBody TipoItemRequest body, 
        @RequestHeader("access_token") String accessToken,            
        HttpServletRequest request
    )
    {
        try {
            LOGGER.info("Calling tipoItemService.update(?, ?)", body, accessToken);
            TipoItemResponse updated = tipoItemService.update(body, accessToken);
            return new ResponseEntity<>(updated, HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }        
    }    
    
    @GetMapping
    public ResponseEntity<?> findAll(
        HttpServletRequest request,
        @RequestHeader("access_token") String accessToken
    )
    {
        try {
            LOGGER.info("Calling tipoItemService.findAll(?)", accessToken);
            List<TipoItemResponse> items = tipoItemService.findAll(accessToken);
            if(items == null || items.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(items, HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(
        @PathVariable(value = "id", required = true) Integer id,
        @RequestHeader("access_token") String accessToken        
    )
    {
        try {
            LOGGER.info("Calling tipoItemService.delete(?, ?)", id, accessToken);
            tipoItemService.delete(id, accessToken);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }
    
    
}