package py.com.pol.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import py.com.pol.model.error.ErrorData;
import py.com.pol.model.error.ResourceError;
import py.com.pol.model.request.RolSistemaRequest;
import py.com.pol.model.response.RolSistemaResponse;
import py.com.pol.service.impl.RolSistemaService;

/**
 *
 * @author Gino Junchaya
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/rol/system")
public class RolSistemaController {
    
    private static final Logger LOGGER = LogManager.getLogger(RolSistemaController.class);    
    
    @Autowired
    private RolSistemaService rolService;
    
    @GetMapping
    public ResponseEntity<?> findAll(HttpServletRequest request) {
        try {
            List<RolSistemaResponse> roles = rolService.findAll();
            if(roles == null || roles.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(roles, HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }
    
    @PostMapping
    public ResponseEntity<?> save(@Valid @RequestBody RolSistemaRequest body, HttpServletRequest request){
        try {
            RolSistemaResponse saved = rolService.save(body);
            return new ResponseEntity<>(saved, HttpStatus.CREATED);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));            
        }
    }
    
    @PutMapping
    public ResponseEntity<?> update(@Valid @RequestBody RolSistemaRequest body, HttpServletRequest request){
        try {
            RolSistemaResponse updated = rolService.update(body);
            return new ResponseEntity<>(updated, HttpStatus.CREATED);            
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));            
        }        
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(
        @PathVariable(value = "id", required = true) Integer id, 
        HttpServletRequest request
    ){
        try {
            rolService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));            
        }        
    }       
    
}
