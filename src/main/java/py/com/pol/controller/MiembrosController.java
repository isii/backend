package py.com.pol.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import py.com.pol.model.error.ErrorData;
import py.com.pol.model.error.ResourceError;
import py.com.pol.model.request.RolesProyectoMiembrosRequest;
import py.com.pol.model.response.RolesProyectoMiembros;
import py.com.pol.service.impl.RolesProyectoMemberService;

/**
 *
 * @author Gino Junchaya
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/projects/members")
public class MiembrosController {
    
    private static final Logger LOGGER = LogManager.getLogger(MiembrosController.class);    
 
    @Autowired
    private RolesProyectoMemberService rolesProyectoMemberService;
    
    @PostMapping
    public ResponseEntity<?> save(
        @Valid @RequestBody RolesProyectoMiembrosRequest body, 
        @RequestHeader("access_token") String accessToken,
        HttpServletRequest request
    )
    {
        try {
            LOGGER.info("Calling rolesProyectoMemberService.save(?, ?)", body, accessToken);
            RolesProyectoMiembros created = rolesProyectoMemberService.save(body, accessToken);
            return new ResponseEntity<>(created, HttpStatus.CREATED);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }        
    }
    
    @PutMapping
    public ResponseEntity<?> update(
        @Valid @RequestBody RolesProyectoMiembrosRequest body, 
        @RequestHeader("access_token") String accessToken,            
        HttpServletRequest request
    )
    {
        try {
            LOGGER.info("Calling rolesProyectoMemberService.update(?, ?)", body, accessToken);
            RolesProyectoMiembros updated = rolesProyectoMemberService.update(body, accessToken);
            return new ResponseEntity<>(updated, HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }        
    }    
    
    @GetMapping
    public ResponseEntity<?> findAll(
        HttpServletRequest request,
        @RequestParam("project_id") Integer projectId,
        @RequestHeader("access_token") String accessToken
    )
    {
        try {
            LOGGER.info("Calling rolesProyectoMemberService.findAll(?, ?)", accessToken);
            List<RolesProyectoMiembros> projects = rolesProyectoMemberService.findAll(accessToken, projectId);
            if(projects == null || projects.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(projects, HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(
        @PathVariable(value = "id", required = true) Integer id,
        @RequestHeader("access_token") String accessToken        
    )
    {
        try {
            LOGGER.info("Calling rolesProyectoMemberService.delete(?, ?)", id, accessToken);
            rolesProyectoMemberService.delete(id, accessToken);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }
    
    
}