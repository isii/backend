package py.com.pol.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import py.com.pol.model.error.ErrorData;
import py.com.pol.model.error.ResourceError;
import py.com.pol.model.request.LineaBaseRequest;
import py.com.pol.model.response.LineaBaseResponse;
import py.com.pol.service.impl.LineaBaseService;

/**
 *
 * @author Gino Junchaya
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/baseline")
public class LineaBaseController {
    
    private static final Logger LOGGER = LogManager.getLogger(LineaBaseController.class);    
 
    @Autowired
    private LineaBaseService lineaBaseService;
    
    @PostMapping
    public ResponseEntity<?> save(
        @Valid @RequestBody LineaBaseRequest body, 
        @RequestHeader("access_token") String accessToken,
        HttpServletRequest request
    )
    {
        try {
            LOGGER.info("Calling lineaBaseService.save(?, ?)", body, accessToken);
            lineaBaseService.save(body, accessToken);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(e.getMessage());
        }        
    }
    
    @GetMapping
    public ResponseEntity<?> findAll(
        HttpServletRequest request,
        @RequestParam("project_id") Integer projectId,
        @RequestHeader("access_token") String accessToken
    )
    {
        try {
            LOGGER.info("Calling lineaBaseService.findAll(?, ?)", accessToken, projectId);
            List<LineaBaseResponse> items = lineaBaseService.findAll(accessToken, projectId);
            if(items == null || items.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(items, HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }    
    
}