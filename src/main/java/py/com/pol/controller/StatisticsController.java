package py.com.pol.controller;

import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import py.com.pol.model.error.ErrorData;
import py.com.pol.model.error.ResourceError;
import py.com.pol.model.response.Statistics;
import py.com.pol.service.impl.StatisticsService;

@CrossOrigin("*")
@RestController
@RequestMapping("/statistics")
public class StatisticsController {

    private static final Logger LOGGER = LogManager.getLogger(StatisticsController.class);

    @Autowired
    private StatisticsService statisticsService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getStatistics(@RequestHeader("access_token") String accessToken,
            HttpServletRequest request) {
        try {
            LOGGER.info("Calling statisticsService.getStatistics(?)", accessToken);
            Statistics statistics = statisticsService.getStatistics(accessToken);
            if (Objects.isNull(statistics)) {
                return ResponseEntity.noContent().build();
            }
            return ResponseEntity.ok(statistics);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }
}
