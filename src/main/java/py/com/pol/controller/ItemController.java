package py.com.pol.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import py.com.pol.model.error.ErrorData;
import py.com.pol.model.error.ResourceError;
import py.com.pol.model.request.ItemRequest;
import py.com.pol.model.request.MoveRequest;
import py.com.pol.model.response.ItemResponse;
import py.com.pol.service.impl.ItemService;

/**
 *
 * @author Gino Junchaya
 */

@CrossOrigin("*")
@RestController
@RequestMapping("/items")
public class ItemController {
    
    private static final Logger LOGGER = LogManager.getLogger(ItemController.class);    
 
    @Autowired
    private ItemService itemService;
    
    @PostMapping
    public ResponseEntity<?> save(
        @Valid @RequestBody ItemRequest body, 
        @RequestHeader("access_token") String accessToken,
        HttpServletRequest request
    )
    {
        try {
            LOGGER.info("Calling itemService.save(?, ?)", body, accessToken);
            itemService.save(body, accessToken);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }        
    }
    
    @PostMapping("/{id}")
    public ResponseEntity<?> saveChild(
        @PathVariable("id") Integer taskId,
        @Valid @RequestBody ItemRequest body, 
        @RequestHeader("access_token") String accessToken,
        HttpServletRequest request
    )
    {
        try {
            LOGGER.info("Calling itemService.save(?, ?)", body, accessToken);
            itemService.addChild(accessToken, taskId, body);
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }        
    }    
    
    @PatchMapping(value = "/move")
    public ResponseEntity<?> move(
        @Valid @RequestBody MoveRequest body,
        @RequestHeader("access_token") String accessToken,            
        HttpServletRequest request
    ){
        try {
            LOGGER.info("Calling itemService.move(?, ?)", body, accessToken);
            itemService.move(body, accessToken);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }                
    }
    
    @PutMapping
    public ResponseEntity<?> update(
        @Valid @RequestBody ItemRequest body, 
        @RequestHeader("access_token") String accessToken,            
        HttpServletRequest request
    )
    {
        try {
            LOGGER.info("Calling itemService.update(?, ?)", body, accessToken);
            ItemResponse updated = itemService.update(body, accessToken);
            return new ResponseEntity<>(updated, HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }        
    }    
    
    @GetMapping("/{id}")
    public ResponseEntity<?> findById(
        HttpServletRequest request,
        @PathVariable("id") Integer taskId,
        @RequestHeader("access_token") String accessToken
    )
    {
        try {
            LOGGER.info("Calling itemService.findById(?, ?)", accessToken, taskId);
            ItemResponse item = itemService.findById(accessToken, taskId);
            if(item == null){
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(item, HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }    
    
    @GetMapping("/{id}/childrens")
    public ResponseEntity<?> findChildren(
        HttpServletRequest request,
        @PathVariable("id") Integer taskId,
        @RequestHeader("access_token") String accessToken
    )
    {
        try {
            LOGGER.info("Calling itemService.findChildren(?, ?)", accessToken, taskId);
            List<ItemResponse> items = itemService.findChildren(accessToken, taskId);
            if(items == null || items.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(items, HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }    
    
    @GetMapping("/tree")
    public ResponseEntity<?> findAllTree(
        HttpServletRequest request,
        @RequestParam("project_id") Integer projectId,
        @RequestHeader("access_token") String accessToken
    )
    {
        try {
            LOGGER.info("Calling itemService.findAllTree(?, ?)", accessToken, projectId);
            List<ItemResponse> items = itemService.findAllTree(accessToken, projectId);
            if(items == null || items.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(items, HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }    
    
    @GetMapping
    public ResponseEntity<?> findAll(
        HttpServletRequest request,
        @RequestParam("project_id") Integer projectId,
        @RequestHeader("access_token") String accessToken
    )
    {
        try {
            LOGGER.info("Calling itemService.findAll(?, ?)", accessToken, projectId);
            List<ItemResponse> items = itemService.findAll(accessToken, projectId);
            if(items == null || items.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(items, HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }
    
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> delete(
        @PathVariable(value = "id", required = true) Integer id,
        @RequestHeader("access_token") String accessToken        
    )
    {
        try {
            LOGGER.info("Calling itemService.delete(?, ?)", id, accessToken);
            itemService.delete(id, accessToken);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        catch(Exception e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
            .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }
    
    
}