package py.com.pol.controller;

import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import py.com.pol.model.request.LoginRequestData;
import py.com.pol.model.response.LoginResponseData;
import py.com.pol.model.exception.ConverterException;

import javax.validation.Valid;
import py.com.pol.model.error.ErrorData;
import py.com.pol.model.error.ResourceError;
import py.com.pol.model.exception.DatabaseAccessException;
import py.com.pol.model.exception.InvalidCredentialsException;
import py.com.pol.model.response.LoginResponse;
import py.com.pol.service.AuthenticationService;

@CrossOrigin("*")
@RestController
@RequestMapping("/authentication")
public class AuthenticationController {

    private static final Logger LOGGER = LogManager.getLogger(AuthenticationController.class);

    private static final String LOGIN_URL = "/login";

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping(value = LOGIN_URL, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity login(@RequestBody @Valid LoginRequestData data, HttpServletRequest request) {
        try {
            LoginResponse response = authenticationService.login(data.getData(), request);
            return new ResponseEntity<>(new LoginResponseData(response), HttpStatus.OK);
        }
        catch (InvalidCredentialsException e) {
            LOGGER.error(e.getMessage(), e);
            return new ResponseEntity(HttpStatus.UNAUTHORIZED);
        }
        catch(DatabaseAccessException | ConverterException e){
            LOGGER.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(new ErrorData(ResourceError.internalServerError(e.getMessage())));
        }
    }

}
